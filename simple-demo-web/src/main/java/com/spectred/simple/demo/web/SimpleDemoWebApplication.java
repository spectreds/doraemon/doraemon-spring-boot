package com.spectred.simple.demo.web;

import com.spectred.aspectlog.annotation.AspectLog;
import com.spectred.aspectlog.config.EnableAspectLog;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author spectred
 */
@RestController
@SpringBootApplication
public class SimpleDemoWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleDemoWebApplication.class, args);
    }

    @AspectLog
    @GetMapping("/{str}")
    public String get(@PathVariable("str") String str) {
        return "Hello " + str;
    }

}
