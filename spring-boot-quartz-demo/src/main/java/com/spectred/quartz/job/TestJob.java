package com.spectred.quartz.job;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.time.LocalDateTime;

/**
 * @author spectred
 */
public class TestJob implements Job {

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        JobDataMap jobDetailDataMap = jobExecutionContext.getJobDetail().getJobDataMap();
        jobDetailDataMap.forEach((k, v) -> System.out.println(k + ":" + v));

        JobDataMap jobTriggerDataMap = jobExecutionContext.getTrigger().getJobDataMap();
        jobTriggerDataMap.forEach((k, v) -> System.out.println(k + ":" + v));

        System.out.println("TestJob execute . time:" + LocalDateTime.now());
    }
}
