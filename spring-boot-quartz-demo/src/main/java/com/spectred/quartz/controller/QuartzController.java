package com.spectred.quartz.controller;

import com.spectred.quartz.job.TestJob;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * @author spectred
 */
@RequestMapping("/quartz")
@RestController
public class QuartzController {

    private final Scheduler scheduler;

    @Autowired
    public QuartzController(Scheduler scheduler) {
        this.scheduler = scheduler;
    }


    @PostMapping("/scheduler/start/{name}")
    public Object start(@PathVariable("name") String name) throws SchedulerException {
        Date start = new Date(System.currentTimeMillis() + 7 * 1000);

        JobDetail jobDetail = JobBuilder.newJob(TestJob.class)
                .usingJobData("name", "jack")
                .usingJobData("age", "20")
                .withIdentity(name)
                .build();

        Trigger trigger = TriggerBuilder.newTrigger()
                .usingJobData("name", name)
                .withIdentity(name)
                .startAt(start)
                .withSchedule(CronScheduleBuilder.cronSchedule("0/5 * * * * ? "))
                .build();

        scheduler.scheduleJob(jobDetail, trigger);

        if (!scheduler.isShutdown()) {
            scheduler.start();
        }

        System.out.println("--- 定时任务启动成功:" + LocalDateTime.now());
        return "ok";
    }

    @PatchMapping("/trigger/pause/{name}")
    public Object pauseTrigger(@PathVariable("name") String name) throws SchedulerException {
        scheduler.pauseTrigger(TriggerKey.triggerKey(name));
        return "ok";
    }

    @PatchMapping("/trigger/resume/{name}")
    public Object resume(@PathVariable("name") String name) throws SchedulerException {
        scheduler.resumeTrigger(TriggerKey.triggerKey(name));
        return "ok";
    }

    @DeleteMapping("/job/{name}")
    public Object delete(@PathVariable("name") String name) throws SchedulerException {
        scheduler.pauseTrigger(TriggerKey.triggerKey(name));

        scheduler.unscheduleJob(TriggerKey.triggerKey(name));

        scheduler.deleteJob(JobKey.jobKey(name));
        return "ok";
    }

    @GetMapping("/job/triggers/{name}")
    public List<? extends Trigger> getTriggersOfJob(@PathVariable("name") String name) throws SchedulerException {
        return scheduler.getTriggersOfJob(JobKey.jobKey(name));
    }
}
