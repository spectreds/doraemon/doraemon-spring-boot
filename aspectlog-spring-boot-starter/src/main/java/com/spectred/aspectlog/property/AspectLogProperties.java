package com.spectred.aspectlog.property;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * aspectlog properties
 * @author spectred
 */
@ConfigurationProperties("aspectlog")
public class AspectLogProperties {

    private boolean enable;

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }
}
