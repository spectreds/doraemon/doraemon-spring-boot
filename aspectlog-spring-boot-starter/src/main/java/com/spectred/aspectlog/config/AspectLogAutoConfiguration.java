package com.spectred.aspectlog.config;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.PriorityOrdered;
import org.springframework.core.annotation.Order;

import java.util.Arrays;

/**
 * ${@link com.spectred.aspectlog.annotation.AspectLog}的实现
 *
 * @author spectred
 */
@Order(PriorityOrdered.LOWEST_PRECEDENCE)
@Aspect
@Configuration
@EnableAspectJAutoProxy(exposeProxy = true, proxyTargetClass = true)
@ConditionalOnProperty(prefix = "aspectlog", name = "enable", havingValue = "true", matchIfMissing = true)
public class AspectLogAutoConfiguration {

    protected Logger log = LoggerFactory.getLogger(getClass());

    @Pointcut("@annotation(com.spectred.aspectlog.annotation.AspectLog)")
    public void pointcut() {
    }

    @Around("pointcut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        String signatureName = joinPoint.getSignature().toString();
        long start = System.currentTimeMillis();
        Object result = joinPoint.proceed();
        log.info("AspectLog:" +
                        "\n -> Method: {}" +
                        "\n  --> Params: {}" +
                        "\n  --> Result: {}" +
                        "\n  --> Run: {}ms",
                signatureName, Arrays.toString(args), result, System.currentTimeMillis() - start);
        return result;
    }


}
