package com.spectred.aspectlog.config;

import org.springframework.context.annotation.Import;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 若不采用SPI被动注入需要的bean,则可用过@Import的方式主动注入需要的bean
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import(AspectLogAutoConfiguration.class)
public @interface EnableAspectLog {
}
